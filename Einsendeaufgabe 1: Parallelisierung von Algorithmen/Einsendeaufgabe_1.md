- **Teil 1**

1. 
Wir haben OpenMP für die Parallelisierung des Programms gewählt.

Mit hilfe von time  command in Linux kann man bestimmen, wie lange die Ausführung eines bestimmten Befehls dauert.
Bei dem C Program dauert es 0,004 Sekunden vor der Parallelisierung.

Bild 1![Nicht_paral](/uploads/aff3c8df783bd246eb8ac098d0e36ab1/Nicht_paral.JPG)

Dann fangen wir mal mit der Parallelisierung.
Zuerst ist es wichtig ( #include <omp.h>) zu verwenden, um die OpenMP Bibliothek aufzurufen.

Nach der Parallelisierung wurden die folgenden Ergebnisse erzielt:

Bild 2![Paral](/uploads/8e59a3111835431df052d68a2b727e55/Paral.JPG)

Das bedeutet, dass wir eine kleine  Zeitverbesserung mit der gegebenen Matrix erreicht haben.

2. Wir haben versucht, das Programm mit einer Matrix von 1440x1440 laufen zu lassen, aber wir erhalten immer die gleiche Zeit-Ergebnisse von 0,004 Sekunden.

-**Teil 2**

Wir haben ein C Program aus https://www.codesansar.com/c-programming-examples/matrix-determinant.htm benutzt.
Das Program soll die Determinante der Matrix herausfinden.
Normaleweise dauert es 18 Sekunden

Bild 3 ![Nicht_paral_deter](/uploads/ba96fd5952abafbb209a5fc1849b4e0a/Nicht_paral_deter.JPG)

Dann haben wir begonnen, das Programm zu parallelisieren.
Nachdem wir das Programm parallelisiert hatten, wurde die Zeit von 18 Sekunden bis auf 8 Sekunden verringert.

Bild 4![Paral_deter](/uploads/fc361169d713675e798eb7c617a3328a/Paral_deter.JPG)

- **Schlussfolgerung**

Bei der Parallelisierung des Householder-Programms gab es keinen großen Unterschied, wir denken, dass dies daran liegt, dass die Aufgaben im Householer-Programm klein sind, aber es kann besser für zeitaufwändigere Funktionen funktionieren.
Beim householder Program gab es mehr Funktionen als die Determinanterechner Program, deswegen glauben wir, dass der Grund war warum man keine deutchliche Unterschied gemerkt hat.

Im zweiten Teil wird für das Programm mehr Zeit in der Prozessor für die Ausführung gebraucht. Deshalb könnte man den Unterschied klar erkennen.


