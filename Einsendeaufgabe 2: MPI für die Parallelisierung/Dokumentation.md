**Teil 1**

1. 
Die Leistung für das sequenzielle Program beträgt: 
Bild 1:  ![Nicht_paral](/uploads/fc9a91818dc4c54e5e2cf1cef9a99e15/Nicht_paral.JPG)

Nach der Parallelisierung des Programs mit Hilfe von MPI haben wir die folgende Zeit erhalten:
Bilde 2: ![1](/uploads/b897cd59f1c5dc3fa4b4e787f167345e/1.JPG)

Die gleiche Zeiten gilten sowohl für große als auch kleine Matrizen.

2. 

- Kosten der Programme:
 Cp= Tp(n)*p, wobei ist p der Anzahl der Prozessoren.

 Sequenziell: 
  Zeit vom Beginn bis zum Ende des Anrufs beträgt: 4 ms
  Menge der im Benutzermodus verbrauchten CPU-Zeit beträgt: 3 ms
  Höhe der im Kernel-Modus verbrauchten CPU-Zeit beträgt: 1 ms 
  Cp= 4ms*1= 4ms

 OpenMP:
  Zeit vom Beginn bis zum Ende des Anrufs beträgt: 3 ms
  Menge der im Benutzermodus verbrauchten CPU-Zeit beträgt: 3 ms
  Höhe der im Kernel-Modus verbrauchten CPU-Zeit beträgt: 0 ms
  Cp= 3ms*1= 3ms

 MPI:
  Zeit vom Beginn bis zum Ende des Anrufs beträgt: 398 ms
  Menge der im Benutzermodus verbrauchten CPU-Zeit beträgt: 27 ms
  Höhe der im Kernel-Modus verbrauchten CPU-Zeit beträgt: 74 ms
  Cp= 398ms*1= 398ms

- Speedup von den zwei parallelen Versionen in Bezug auf das seqzuenzielle:
 Die durch den Einsatz von n CPUs gewonnene beschlleunigun, Speedup(n), ist das Verhältnis der Ausführungszeit mit 
 einer CPU zur parallelen Ausführungszeit mit n CPUs: Speedup(n) = T(1)/T(n).

 Wir haben auf diesem Rechner 1 CPU core

 für OpenMP: Speedup = 4/3 = 1,33, Hier handelt es sich um ein superlinear Speedup weil der Speedup ist größer als der 
 CPU anzahl.

 für MPI: Speedup = 4/398 = 0,01, Nicht linear weil der Speedup beträgt 0,010.



- Effizienz von den zwei parallelen Versionen
 Effizienz ist alternativ zum Speedup, es ist ein Anteil der Laufzeit, den ein Prozessor für Berechnungen benötigt, 
 die auch im sequentiellen Programm enthalten sind. Ep(n)= Sp(n)/p

 Für OpenMP: Sp = 1,33
 Ep= 1,33/1 = 1,33

 Für MPI: Sp= 0,010
 Ep= 0,010/1 = 0,010

Die Parallelisierung des Programms mit Hilfe von MPI war nicht so erfolgreich wie die Parallelisierung mit OpenMP.
Dies lässt sich anhand der berechneten Kosten, der Speedup und der Effizienz feststellen.

**Teil 2**

- Kosten der Programme:
  Cp= Tp(n)*p

  Sequenziell: 18445 ms
  C= 18445 ms* 1= 18445 ms
  
  OpenMP: 8958 ms
  C= 8958 ms* 1= 8958 ms

  MPI: 20328 ms
  C= 20328 ms* 1= 20328 ms

- Speedup
Speedup(n) = T(1)/T(n)

 OpenMP: 
 Speedup= 18445 ms/8958 ms= 2,06
 
 MPI:
 Speedup= 18445 ms/20328 ms= 0,907

- Effizienz 
Ep(n)= Sp(n)/p

 OpenMP:
 Ep= 2,06/1 = 2,06 -> Superlinear Speedup

 MPI:
 Ep= 0,907/1 = 0,907 -> Nicht linear

Die Ergebnisse zeigen, dass die Parallelisierung mit OpenMP sehr erfolgreich war. Während mit Hilfe von MPI das Ergebnis nicht sehr optimal war.
